import AVKit
import CoreImage
import Foundation

struct SampleBufferTransformer {
    func transform(videoSampleBuffer: CMSampleBuffer) -> CMSampleBuffer {
        guard let pixelBuffer = CMSampleBufferGetImageBuffer(videoSampleBuffer) else {
            print("failed to get pixel buffer")
            fatalError()
        }
        
        let originalImage = CIImage(cvPixelBuffer: pixelBuffer)
        let invertedColorsImage = colorInvert(inputImage: originalImage)
        
        let invertedBuffer = self.pixelBuffer(from: invertedColorsImage)

        guard let result = try? pixelBuffer.mapToSampleBuffer(timestamp: videoSampleBuffer.presentationTimeStamp) else {
            fatalError()
        }

        return result
    }
    
    private func colorInvert(inputImage: CIImage) -> CIImage {
        guard let colorInvertFilter = CIFilter(name: "CIColorInvert") else {
            fatalError()
        }
        colorInvertFilter.setValue(inputImage, forKey: kCIInputImageKey)
        return colorInvertFilter.outputImage!
    }
    
    private func pixelBuffer(from image: CIImage) -> CVPixelBuffer? {
        var pixelBuffer: CVPixelBuffer?
        CVPixelBufferCreate(
            kCFAllocatorDefault,
            Int(image.extent.size.width),
            Int(image.extent.size.height),
            kCVPixelFormatType_32BGRA,
            nil,
            &pixelBuffer
        )
        guard let pixelBuffer else {
            return nil
        }
        CIContext().render(image, to: pixelBuffer)
        return pixelBuffer
    }

}
